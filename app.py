from flask_api import FlaskAPI
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS
from sqlalchemy import text
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity


app = FlaskAPI(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

# app.secret_key = os.uran3om(24)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI']='postgres://ulgrjbqdmlzlam:f87681e5d52dc209c2867dec9734e68978c3e11ec990cec837f25f2f6009381e@ec2-54-165-164-38.compute-1.amazonaws.com:5432/dbhapsljo4h5uu'
db = SQLAlchemy(app)

datafull= []
@app.route("/",methods=['GET'])
def home():
    return {
        'data': 'hello'
    }

@app.route("/api/home",methods=['GET'])
def get_news_home():
    statement = text("SELECT * FROM posts ORDER BY title DESC LIMIT 15")
    dataPost = db.engine.execute(statement)
    post = [{'id':row[0],'idCategory' :row[1],'title':row[3],'Content': row[5], 'Tag':row[7],'image': row[6]} for row in dataPost]
    statement2 = text("SELECT * FROM posts ORDER BY idcategory LIMIT 6")
    dataPost2 = db.engine.execute(statement2)
    post2 = [{'id':row[0],'idCategory' :row[1],'title':row[3],'Content': row[5], 'Tag':row[7],'image': row[6]} for row in dataPost2]
    return jsonify(post ,post2)

@app.route("/api/hotnews",methods=['GET'])
def get_hotnews():
    result = db.engine.execute("SELECT * FROM posts ORDER BY id DESC LIMIT 6")
    data = [{'id':row[0],'idCategory' :row[1],'title':row[3], 'Content':row[5],'image': row[6]} for row in result]
    return jsonify(data)

@app.route("/api/GetCategory/<id>",methods=['GET'])
def get_category_Detail(id):
    result = db.engine.execute("SELECT * FROM categorys  WHERE id = " + id)
    data = [{'idCategory' :row[0],'name':row[1]} for row in result]
    return jsonify(data)

@app.route("/api/post",methods=['GET'])
def get_news():
    result = db.engine.execute("SELECT * FROM posts")
    data = [{'id':row[0],'idCategory' :row[1],'title':row[3], 'Content':row[5],'image': row[6]} for row in result]
    return jsonify(data)


@app.route("/api/post/<id>",methods=['GET'])
def get_post_Detail(id):
    #get full data gom id, title, namecategory
    result = db.engine.execute("SELECT posts.id, posts.title,categorys.name, posts.tag FROM posts INNER JOIN categorys ON posts.idcategory=categorys.id")
    data = [{'id':row[0],'title' :row[1],'namecategory':row[2],'tag':row[3]} for row in result]
    #get data cua 1 bai post theo id
    statement = text("SELECT * FROM posts WHERE id = " + id )
    dataPost = db.engine.execute(statement)
    post = [{'id':row[0],'idCategory' :row[1],'title':row[3],'Content': row[5], 'Tag':row[7],'image': row[6]} for row in dataPost]
    #XU LI LOGIC de lay nhung post lien quan voi bai post
    def get_title_from_index(id):
	    return df[df.id == id]["title"].values[0]
    def get_index_from_title(title):
	    return df[df.title == title]["id"].values[0]
    def combine_features(row):
        try:
            return row['title']+"" +row['namecategory']
        except:
            print ("Error:", row)	
    dataId= []
    dataTitle=[]
    nameCategory = []
    tag= []
    for d in data:
        dataId.append(d['id'])
        dataTitle.append(d['title'])
        nameCategory.append(d['namecategory'])
        tag.append(d['tag'])

    C = {'id':dataId,
        'title': dataTitle,
        'namecategory': nameCategory,
        'tag':tag
    }
    df = DataFrame(C, columns= ['id', 'title', 'namecategory','tag'])
    export_csv = df.to_csv (r'result.csv', index = None, header=True)
    df = pd.read_csv("result.csv")

    features = ['title','namecategory','tag']
    for feature in features:
	    df[feature] = df[feature].fillna('')

    df["combine_string"] = df.apply(combine_features,axis=1)
    cv = CountVectorizer()
    count_matrix = cv.fit_transform(df["combine_string"])   
    cosine_sim = cosine_similarity(count_matrix) 
    title = post[0]['title']
    post_index = get_index_from_title(title)

    similar_posts =  list(enumerate(cosine_sim[post_index]))
    sorted_similar_posts = sorted(similar_posts,key=lambda x:x[1],reverse=True)
    i=0
    postRs = []
    for element in sorted_similar_posts:
        # print(get_title_from_index(element[0]))
        postRs.append(element[0])
        i=i+1
        if i>6:
            break
    dataRS = []
    for i in range(0,len(postRs)):
        statement = text("SELECT * FROM posts WHERE id = " + str(postRs[i])) 
        dataPost = db.engine.execute(statement)
        postResult = [{'id':row[0],'idCategory' :row[1],'title':row[3],'Content': row[5], 'Tag':row[7],'image': row[6]} for row in dataPost]
        dataRS.append(postResult[0])
    print(dataRS)
    return jsonify(dataRS)
    # return jsonify(data)


if __name__ == "__main__":
    app.run()

